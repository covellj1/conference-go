from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests

# import json


def get_pic_url(city, state):
    # send GET request to pexel to get pic of city/state
    params = {"query": city + ", " + state, "per_page": 1}
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    if response.status_code == 200:
        response = response.json()
        if response.get("photos"):
            pic_url = response["photos"][0]["src"]["original"]
            return pic_url
        else:
            return None
    else:
        return None


def get_weather(city, state):
    params = {
        "q": city + ", " + state,
        "appid": OPEN_WEATHER_API_KEY,
    }
    geoloc = requests.get(
        "http://api.openweathermap.org/geo/1.0/direct", params=params
    )
    if geoloc.status_code == 200:
        geoloc = geoloc.json()
        params = {
            "appid": OPEN_WEATHER_API_KEY,
            "lat": geoloc[0]["lat"],
            "lon": geoloc[0]["lon"],
            "units": "imperial",
        }
        weather = requests.get(
            "https://api.openweathermap.org/data/2.5/weather", params=params
        )
        if weather.status_code == 200:
            weather = weather.json()

            description = weather["weather"][0]["description"]
            temp = weather["main"]["temp"]

            return description, temp
        else:
            return None, None
    else:
        return None, None
