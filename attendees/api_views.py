from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee
from events.models import Conference
from events.api_views import ConferenceListEncoder
from django.views.decorators.http import require_http_methods
import json


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)

        return JsonResponse(
            {"attendees": attendees}, encoder=AttendeeListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"Message": "Invalid conference id"}, status=400
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }


@require_http_methods(["GET", "DELETE", "PUT", "PATCH"])
def api_show_attendee(request, id):
    try:
        if request.method == "GET":
            attendee = Attendee.objects.get(id=id)
            return JsonResponse(
                attendee, encoder=AttendeeDetailEncoder, safe=False
            )
        elif request.method == "DELETE":
            count, _ = Attendee.objects.get(id=id).delete()
            return JsonResponse({"Deleted": count > 0})
        else:
            content = json.loads(request.body)
            conference = Conference.objects.get(id=content["conference"])
            content["conference"] = conference
            Attendee.objects.filter(id=id).update(**content)
            attendee = Attendee.objects.get(id=id)
            return JsonResponse(attendee, AttendeeDetailEncoder, False)
    except Attendee.DoesNotExist:
        return JsonResponse({"Message": "Invalid Attendee id"})
    except Conference.DoesNotExist:
        return JsonResponse({"Message": "Invalid Conference id"})
